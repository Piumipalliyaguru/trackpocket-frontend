import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";
import "./AppHeader.css";

import { Layout, Menu, Dropdown, Icon } from "antd";
const Header = Layout.Header;

class AppHeader extends Component {
  constructor(props) {
    super(props);
    this.handleMenuClick = this.handleMenuClick.bind(this);
  }

  handleMenuClick({ key }) {
    if (key === "logout") {
      this.props.onLogout();
    }
  }

  render() {
    let menuItems;
    if (this.props.currentUser) {
      menuItems = [
        <Menu.Item key="/">
          <Link to="/">
            <Icon type="home" className="nav-icon" />
          </Link>
        </Menu.Item>,
        <Menu.Item key="/poll/new">
          <Link to="/poll/new">
            {/* <img src={pollIcon} alt="poll" className="poll-icon" /> */}
          </Link>
        </Menu.Item>,
        <Menu.Item key="/profile" className="profile-menu">
          {/* <ProfileDropdownMenu
            currentUser={this.props.currentUser}
            handleMenuClick={this.handleMenuClick}
          /> */}
        </Menu.Item>
      ];
    } else {
      menuItems = [
        <Menu.Item key="/login">
          <Link to="/login">Login</Link>
        </Menu.Item>,
        <Menu.Item key="/signup">
          <Link to="/signup">Signup</Link>
        </Menu.Item>
      ];
    }

    return (
      <Header className="app-header">
        <div className="container">
          <div className="app-logo">
            <Link to="/">
              <img src="./image/logo.svg" />
            </Link>
          </div>
          <div className="app-title">
            <Link to="/">trackPOCKET</Link>
          </div>
          <Menu
            className="app-menu"
            mode="horizontal"
            selectedKeys={[this.props.location.pathname]}
            style={{ lineHeight: "64px" }}
          >
            {/* <Link to="/login">Login</Link>

            <Link to="signup">Signup</Link> */}

            {menuItems}
          </Menu>
        </div>
      </Header>
    );
  }
}

export default withRouter(AppHeader);
